#ifndef KSTING_H
#define KSTING_H

#include <iostream>
#include <string.h>
#include <stdio.h> //sprintf
#include <exception>
#include <vector>

typedef unsigned short kchar_t;  //two bytes

namespace constants
{
    constexpr short NUM_BITS {8};
}
#define MAX_INT_DIGITS 12

/* ********************** */
/* ********************** */
/* ********************** */

/* global functions for the unicode to UTF-8 conversion */

/* ********************** */
/* ********************** */
/* ********************** */

/*
   Char. number range  |        UTF-8 octet sequence
      (hexadecimal)    |              (binary)
   --------------------+---------------------------------------------
   0000 0000-0000 007F | 0xxxxxxx
   0000 0080-0000 07FF | 110xxxxx 10xxxxxx
   0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
   0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx

   Encoding a character to UTF-8 proceeds as follows:

   1.  Determine the number of octets required from the character number
       and the first column of the table above.  It is important to note
       that the rows of the table are mutually exclusive, i.e., there is
       only one valid way to encode a given character.

   2.  Prepare the high-order bits of the octets as per the second
       column of the table.

   3.  Fill in the bits marked x from the bits of the character number,
       expressed in binary.  Start by putting the lowest-order bit of
       the character number in the lowest-order position of the last
       octet of the sequence, then put the next higher-order bit of the
       character number in the next higher-order position of that octet,
       etc.  When the x bits of the last octet are filled in, move on to
       the next to last octet, then to the preceding one, etc. until all
       x bits are filled in.
*/

//UNICODE: https://www.compart.com/en/unicode
//UTF-8  : https://tools.ietf.org/html/rfc3629

//Hex editor ghex

                                  //UTF8        UTF16     UTF32         Decimal
//Example 1:
//latin lowercase letter z        //0x7A        0x007A    0x0000007A    122
//Example 2:
//russian lowercase letter f: ф   //0xD1 0x84   0x0444    0x00000444    1092


static void printBits(kchar_t a)
{
    for (short i = (sizeof (a) * constants::NUM_BITS - 1); i >= 0; i--)
        printf ((a & (1 << i)) ? "1 " : "0 ");
}

//determine the number of bytes required to store resulted UTF-8 string
static int getnumOctetsRequired (kchar_t unicodeChar)
{
    if (unicodeChar <= 0x7F)
        return 1;
    else if (unicodeChar <= 0x7FF)
        return 2;
    else if (unicodeChar <= 0xFFFF)
        return 3;
    else
        return 4;
}

//prepare the bytes to be UTF-8 compatible
static void formHeader(char *str, int numOctetsRequired)
{
    for (int ii = 0; ii < numOctetsRequired; ii++)
    {
        str[ii] = 0;
    }
    for (int i = 0; i < numOctetsRequired; i++)
    {
        str[0] = str[0] | 1 << (7 - i); //8 bits in a byte
        if ((numOctetsRequired - i) > 1)
            str[i + 1] = 1 << 7;
    }
}

//extracting single bits from the unicode character
static int getNextBit2Bytes (kchar_t unicodeChar, int index)
{
    //16 bits in the type kchar_t
    return ((unicodeChar & (1 << (sizeof (kchar_t) * constants::NUM_BITS - index))) ? 1 : 0);
}

//extracting single bits from char
static int getNextBitChar(char c, int index)
{
    //8 bits in the type char
    return ((c & (1 << (sizeof (char) * constants::NUM_BITS - index))) ? 1 : 0);
}

//form the resulting UTF-8 bytes bit-by-bit
static void fillBits(char *str, int numOctetsRequired, kchar_t unicodeChar)
{
    int index = 16; //16 bits in the type kchar_t
    for (int i = numOctetsRequired - 1; i >= 0; i--)
    {
        if (i == 0)
        {
            for (int j = 0; j < (6 - (numOctetsRequired - 1)); j++)
            {
                bool bit = getNextBit2Bytes(unicodeChar, index--);
                str[i] = str[i] | bit << j;
            }
        }
        else
        {
            for (int j = 0; j < 6; j++)
            {
                bool bit = getNextBit2Bytes(unicodeChar, index--);
                if (bit)
                    str[i] = str[i] | bit << j;
            }
        }
    }
}

//input: unicode character, output: UTF-8 char array
char* convertUnicode2UTF8 (kchar_t unicodeChar) //null terminated !!!
{
    const int numOctetsRequired = getnumOctetsRequired (unicodeChar);
    char *str = new char[numOctetsRequired + 1];

    if (numOctetsRequired == 1) //special case: ASCII compatible
        str[0] = (char) unicodeChar;
    else
    {
        formHeader(str, numOctetsRequired);
        fillBits(str, numOctetsRequired, unicodeChar);
    }

    str[numOctetsRequired] = '\0';
    return str;
}

//input: UTF-8 char array, output: unicode character
void convertUTF8_2Unicode (const char* UFT8String,
                           std::vector<kchar_t> &unicodeStr)
{
    kchar_t buffer;

    for (int i = 0; i <= strlen(UFT8String); i++)
    {
        if (UFT8String[i] == '\0') //null sring terminator
            break;

       //special case: ASCII compatible byte
        else if ((UFT8String[i] & (1 << 7)) == 0)
            unicodeStr.push_back ((kchar_t) UFT8String[i]);

        else if ((UFT8String[i] & (1 << 5)) == 0) //two bytes
        {
            buffer = 0;
            //first byte
            for (int j = 8, indexBuffer = 6; j > 3; j--)
            {
                bool bit = getNextBitChar(UFT8String[i], j);
                buffer = buffer | bit << indexBuffer++;
            }
            //second byte
            i++;
            for (int x = 8, indexBuffer = 0; x > 2; x--)
            {
                bool bit = getNextBitChar(UFT8String[i], x);
                buffer = buffer | bit << indexBuffer++;
            }
            unicodeStr.push_back (buffer);
        }

        else if ((UFT8String[i] & (1 << 5)) != 0) //three bytes
        {
            buffer = 0;
            //first byte
            for (int j = 8, indexBuffer = 12; j > 4; j--)
            {
                bool bit = getNextBitChar(UFT8String[i], j);
                buffer = buffer | bit << indexBuffer++;
            }
            //second byte
            i++;
            for (int x = 8, indexBuffer = 6; x > 2; x--)
            {
                bool bit = getNextBitChar(UFT8String[i], x);
                buffer = buffer | bit << indexBuffer++;
            }
            //third byte
            i++;
            for (int x = 8, indexBuffer = 0; x > 2; x--)
            {
                bool bit = getNextBitChar(UFT8String[i], x);
                buffer = buffer | bit << indexBuffer++;
            }
            unicodeStr.push_back (buffer);
        }
    }
}

/* ********************** */
/* ********************** */
/* ********************** */

/* generic (template) implementation <CharT> */

/* ********************** */
/* ********************** */
/* ********************** */

template <typename CharT>
class KBasicString
{
private:
    std::vector<CharT> stringArray;
    char *buffer; //buffer char array for the

    //C-style string representation
    //const char *KBasicString<kchar_t>::c_str () const

public:
    /* operator(s) as global friend function(s) */
    friend KBasicString operator+ (const KBasicString& left,
                                   const KBasicString& right)
    {
        KBasicString result = left;
        result.stringArray.insert (result.stringArray.begin(),
                                   right.stringArray.begin(),
                                    right.stringArray.end());
        return result;
    }

    /* ctors */
    KBasicString () //default ctor
    : buffer(NULL)
    {}

    KBasicString (const char *str);

    KBasicString (const CharT &symbol)
    : buffer(NULL)
    {
        stringArray.push_back (symbol);
    }

    KBasicString (const KBasicString& other) //copy ctor
    : buffer(NULL)
    {
        if(this == &other) return;  //handle self assignment
        stringArray = other.stringArray;
    }

    /* dtors */
    ~KBasicString ()
    {
        if (buffer != NULL)
            delete[] buffer;
    }

    //methods

    const std::vector<CharT> &getString () const {return stringArray;}

    size_t length () const
    {
        return stringArray.size ();
    }

    size_t size () const //same as length
    {
        return length ();
    }

    void clear ()
    {
        stringArray.clear ();
    }

    const bool empty () const
    {
        return stringArray.empty ();
    }

    const char *c_str () //C-style string representation
    {
        return NULL;
    }

    const CharT &at (int index) const
    {
        return stringArray[index];
    }

    const CharT &front () const //access first character
    {
        return stringArray.front ();
    }

    const CharT &back () const //access last character
    {
        return stringArray.back ();
    }

    const KBasicString<CharT>& append (const CharT& c)
    {
        *this += c;
        return *this;
    }

    KBasicString<CharT>& append (CharT& c)
    {
        *this += c;
        return *this;
    }

    void reverse ()
    {
        int left = 0;
        int right = length () - 1;

        while (right > left)
        {
            CharT tmp;
            tmp = stringArray[right];
            stringArray[right--] = stringArray[left];
            stringArray[left++] = tmp;
        }
    }

    KBasicString<CharT>& operator= (const KBasicString<CharT>& other)
    {
        if (this == &other)
            return *this;  //handle self assignment
        this->~KBasicString<CharT> ();
        //placement new: constructs (call ctor) an object
        //(of type KBasicString<CharT>)
        // on memory that's already allocated (this)
        return *(new (this) KBasicString<CharT> (other));
    }

    const CharT& operator[] (int index) const
    {
        /*
        if(index < 0 || index >= length ())
            throw out_of_range(string("The index ") +  string("? is invalid."));
        */
        return stringArray[index];
    }

    CharT& operator[] (int index)
    {
        /*
        if(index < 0 || index >= length ())
            throw out_of_range (string("The index ") + string("? is invalid."));
        */
        return stringArray[index];
    }

    KBasicString<CharT>& operator+= (const KBasicString<CharT>& str)
    {
        stringArray.insert (stringArray.end(), str.stringArray.begin(),
                                               str.stringArray.end());
        return *this;
    }
};

/* operators as global functions */

template <typename CharT>
bool operator== (const KBasicString<CharT>& left,
                 const KBasicString<CharT>& right)
{
    if (&left == &right) //handle self comparement
        return true;
    if (left.length () != right.length ())
        return false; //compare string lengths of both strings
    int length = left.length ();
    for(int i = 0; i < length; i++)
        if (left[i] != right[i]) //T must overload operator!=
            return false;
    return true;
}

template <typename CharT>
bool operator!= (const KBasicString<CharT>& left,
                 const KBasicString<CharT>& right)
{
    return !(left == right);
}

template <typename CharT>
bool operator< (const KBasicString<CharT>& left,
                const KBasicString<CharT>& right)
{
    if (&left == &right) //handle self comparement
        return false;
    int leftLength = left.length ();
    int rightLength = right.length ();
    for (int i = 0; i < leftLength, i < rightLength; i++)
        if (left[i] < right[i]) //CharT must overload operator<
            return true;
    return false;
}

template <typename CharT>
bool operator<= (const KBasicString<CharT>& left,
                 const KBasicString<CharT>& right)
{
    return (left < right || left == right);
}

template <typename CharT>
bool operator> (const KBasicString<CharT>& left,
                const KBasicString<CharT>& right)
{
    return !(left <= right);
}

template <typename CharT>
bool operator>= (const KBasicString<CharT>& left,
                 const KBasicString<CharT>& right)
{
    return !(left < right);
}

template <typename CharT>
std::ostream &operator<< (std::ostream &os, const KBasicString<CharT> &str)
{
   const std::vector<CharT> &v = str.getString ();
   os << "[";
   for (typename std::vector<CharT>::const_iterator iter = v.begin ();
        iter != v.end (); ++ iter)
   {
       os << " " << *iter;
   }
   os << "]";

   return os;
}

/* ********************** */
/* ********************** */
/* ********************** */

/* specialization <char> */

/* ********************** */
/* ********************** */
/* ********************** */

template<>
class KBasicString<char>
{
private:
    char *str_;

public:
    /* operators as global friend functions - specialization <char> */
    friend KBasicString operator+ (const KBasicString& left,
                                   const KBasicString& right)
    {
        KBasicString result;
        delete[] result.str_;
        result.str_ = new char[strlen(left.c_str ())
                      + strlen(right.c_str ()) + 1];
        stpcpy(result.str_, left.c_str ());
        strcat(result.str_, right.c_str ());
        return result;
    }

    //ctors
    KBasicString<char> () //default ctor
    {
        str_ = new char[1];
        str_[0] = '\0';
    }

    KBasicString<char> (const char *str) //const char ctor
    {
        if (str == NULL)
        {
            str_ = new char[1];
            str_[0] = '\0';
        }
        else
        {
            size_t size = strlen(str);
            str_ = new char[size + 1];
            stpcpy (str_, str);
        }
    }

    KBasicString<char> (int i) //integer ctor
    {
        char buffer[MAX_INT_DIGITS];
        sprintf (buffer, "%d", i);
        str_ = new char[strlen(buffer)+1];
        strcpy (str_, buffer);
    }

    KBasicString<char> (const kchar_t &unicodeSymbol) //unicode ctor
    {
        if (unicodeSymbol == 0)
        {
            str_ = new char[1];
            str_[0] = '\0';
        }
        else
        {
            str_ = convertUnicode2UTF8 (unicodeSymbol);
        }
    }

    KBasicString<char> (const KBasicString<char>& other) //copy ctor
    {
        if(this == &other) return;  //handle self assignment
        size_t size = strlen (other.str_);
        str_ = new char[size + 1];
        stpcpy (str_, other.str_);
    }

    // dtors
    ~KBasicString<char> ()
    {
        delete[] str_;
    }

    //methods

    size_t length () const
    {
        return strlen(str_);
    }

    size_t size () const //same as length
    {
        return this->length();
    }

    void clear()
    {
        delete[] str_; //this->~KBasicString<char>();
        str_ = new char[1];
        str_[0] = '\0';
    }

    const bool empty () const
    {
        return *str_ == 0;
    }

    const char *c_str () const //C-style string representation
    {
        return str_;
    }

    char *c_str () //C-style string representation
    {
        return str_;
    }

    const char &at (int index) const
    {
        return str_[index];
    }

    const char &front () const //access first character
    {
        return str_[0];
    }

    const char back () const //access last character
    {
        return empty () ? 0 : str_[(length()-1)];
    }

    const KBasicString<char>& append (const char* s)
    {
        return *this += s;
    }

    void reverse ()
    {
        int left = 0;
        int right = length () - 1;

        while (right > left)
        {
            char tmp;
            tmp = str_[right];
            str_[right--] = str_[left];
            str_[left++] = tmp;
        }
    }

    //copy operator
    KBasicString<char>& operator= (const KBasicString<char>& other)
    {
        if(this == &other)
            return *this;  //handle self assignment
        this->~KBasicString<char>();

        //placement new: constructs (call ctor) an object (of type KString)
        //on memory that's already allocated (this)
        return (*new (this) KBasicString<char>(other));
    }

    const char& operator[] (int index) const
    {
        //if (index < 0 || index >= length ())
            //throw out_of_range (string ("The index ") + tostr_ing (index)
                  //+ string (" is invalid."));
        return str_[index];
    }

    char& operator[] (int index)
    {
        //if(index < 0 || index >= length ())
            //throw out_of_range (string ("The index ") + tostr_ing (index)
                  //+ string (" is invalid."));
        return str_[index];
    }

    KBasicString<char>& operator+= (const KBasicString<char>& other)
    {
        char *str_Tmp = str_;
        str_ = new char[strlen(str_Tmp) + strlen(other.c_str ()) + 1];
        stpcpy(str_, str_Tmp);
        strcat(str_, other.c_str ());
        delete[] str_Tmp;
        return *this;
    }
};

/* operators as global functions - specialization <char> */

inline std::ostream& operator<< (std::ostream &os, const KBasicString<char>& str)
{
    os << str.c_str ();
    return os;
}

inline bool operator== (const KBasicString<char>& left,
                        const KBasicString<char>& right)
{
    return strcmp(left.c_str (), right.c_str ()) == 0;
}

inline bool operator!= (const KBasicString<char>& left,
                        const KBasicString<char>& right)
{
    return !(left == right);
}

inline bool operator< (const KBasicString<char>& left,
                       const KBasicString<char>& right)
{
    return strcmp (left.c_str (), right.c_str ()) < 0;
}

inline bool operator<= (const KBasicString<char>& left,
                        const KBasicString<char>& right)
{
    return strcmp (left.c_str (), right.c_str ()) <= 0;
}

inline bool operator> (const KBasicString<char>& left,
                       const KBasicString<char>& right)
{
    return !(left <= right);
}

inline bool operator>= (const KBasicString<char>& left,
                        const KBasicString<char>& right)
{
    return !(left > right);
}


/* ********************** */
/* ********************** */
/* ********************** */

/* specialization <kchar_t> (unsigned short 2 bytes) */

/* ********************** */
/* ********************** */
/* ********************** */

template<>
KBasicString<kchar_t>::KBasicString (const char *str)
: buffer(NULL)
{
    convertUTF8_2Unicode (str, stringArray);
}

template<>
const char *KBasicString<kchar_t>::c_str () //C-style string representation
{
    int sumBytesRequired = 0;
    for(kchar_t c: stringArray) //determine the size of the buffer char array
        sumBytesRequired += getnumOctetsRequired(c);

    if (buffer != NULL)
        delete[] buffer;

   //buffer char array + 1 byte string terminator 0
   buffer = new char[sumBytesRequired + 1];
   buffer[0] = '\0';

    for(kchar_t c: stringArray)
        strcat(buffer, convertUnicode2UTF8 (c));

    return buffer;
}

#endif // KSTING_H

