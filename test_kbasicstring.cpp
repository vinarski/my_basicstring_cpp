#include <iostream>
#include <string>
#include "kbasicstring.h"

class TestChar
{
    private:
    int value;

    public:

    TestChar (): value() {}

    TestChar (int value): value (value) {}

    int getValue () const {return value;}

     //const promisses not to modify this
    bool operator!= (const TestChar& other) const {return value != other.value;}

    bool operator< (const TestChar& other) const {return value < other.value;} 

    friend std::ostream& operator<< (std::ostream &os, const TestChar& c);

};

std::ostream& operator<< (std::ostream &os, const TestChar& c)
{
    os << c.value;
    return os;
}

int main ()
{
#if 1
    std::cout << " ------------ test printBits -------------- \n";
    kchar_t bits {0b0001'0001'0001'0001};
    std::cout << "bits in dec: " << bits << " bits in binary: ";
    printBits(bits);
    printf ("\n");

    std::cout << " ----------------------------------- \n";
    std::cout << "------ KBasicString<int> ---- \n";

    int i = 12345;
    KBasicString<int> strInt (i);
    KBasicString<int> strInt2 (7);
    KBasicString<int> strInt3 (9);

    strInt.append (6);
    strInt2.append (8);
    strInt = strInt + strInt2;
    strInt+= strInt3;

    std::cout << "Length: " << strInt.length () << "\n";
    std::cout << "strInt[1]: " << strInt[1] << "\n";
    std::cout << "strInt[2]: " << strInt[2] <<  "\n";
    std::cout << "strInt.front (): " << strInt.front () << "\n";
    std::cout << "strInt.back (): " << strInt.back () << "\n";
    std::cout << "strInt: " << strInt << "\n";
    strInt.clear();
    std::cout << "---- After clear () ---- " << "\n";
    std::cout << "Length: " << strInt.length () << "\n";
    std::cout << "strInt.empty (): " << (strInt.empty ()?"TRUE":"FALSE") << "\n";
    std::cout << "strInt[-2] : " << strInt[-2] << "\n";
    std::cout << "strInt[1]: " << strInt[1] << "\n";
    std::cout << "strInt[2]: " << strInt[2] << "\n";
    std::cout << "strInt.front (): " << strInt.front () << "\n";
    std::cout << "strInt.back (): " << strInt.back () << "\n";


    std::cout << "----------------------------------- \n";
    std::cout << "------ KBasicString<TestChar> ---- \n";

    TestChar tc (12345);
    KBasicString<TestChar> strTestChar (tc);
    KBasicString<TestChar> strTestChar2 (TestChar (7));
    KBasicString<TestChar> strTestChar3 (TestChar (9));

    strTestChar.append (6);
    strTestChar2.append (8);
    strTestChar = strTestChar + strTestChar2;
    strTestChar += strTestChar3;

    std::cout << "Length: " << strTestChar.length () << "\n";
    std::cout << "strTestChar[1]: " << strTestChar[1] << "\n";
    std::cout << "strTestChar[2]: " << strTestChar[2] << "\n";
    std::cout << "strTestChar.front (): " << strTestChar.front () << "\n";
    std::cout << "strTestChar.back (): " << strTestChar.back () << "\n";
    std::cout << "strTestChar: " << strTestChar << "\n";

    strTestChar.reverse ();
    std::cout << "---- After reverse () ---- " << "\n";
    std::cout << "Length: " << strTestChar.length () << "\n";
    std::cout << "strTestChar[1]: " << strTestChar[1] << "\n";
    std::cout << "strTestChar[2]: " << strTestChar[2] << "\n";
    std::cout << "strTestChar[3]: " << strTestChar[3] << "\n";
    std::cout << "strTestChar.front (): " << strTestChar.front () << "\n";
    std::cout << "strTestChar.back (): " << strTestChar.back () << "\n";
    std::cout << "strTestChar: " << strTestChar << std::endl;


    strTestChar.clear ();
    std::cout << "---- After clear () ---- " <<  std::endl;
    std::cout << "Length: " << strTestChar.length () <<  std::endl;
    std::cout << "strTestChar.empty (): " << (strTestChar.empty () ? "TRUE" : "FALSE") <<  std::endl;
    std::cout << "strTestChar[-2] : " << strTestChar[-2] << std::endl;
    std::cout << "strTestChar[1]: " << strTestChar[1] <<  std::endl;
    std::cout << "strTestChar[2]: " << strTestChar[2] <<  std::endl;
    std::cout << "strTestChar.front (): " << strTestChar.front () <<  std::endl;
    std::cout << "strTestChar.back (): " << strTestChar.back () <<  std::endl;


    strTestChar = strTestChar + strTestChar2;
    strTestChar += strTestChar3;
    std::cout << "strTestChar: " << strTestChar << std::endl;
    std::cout << "strTestChar2: " << strTestChar2 << std::endl;
    std::cout << "strTestChar ==  strTestChar2: "
         << (strTestChar ==  strTestChar2 ? "TRUE" : "FALSE") << std::endl;
    std::cout << "strTestChar ==  strTestChar: "
         << (strTestChar ==  strTestChar ? "TRUE" : "FALSE") << std::endl;
    std::cout << "strTestChar !=  strTestChar2: "
         << (strTestChar !=  strTestChar2 ? "TRUE" : "FALSE") << std::endl;
    std::cout << "strTestChar <  strTestChar2: "
         << (strTestChar <  strTestChar2 ? "TRUE" : "FALSE") << std::endl;
    std::cout << "strTestChar2 <  strTestChar: "
         << (strTestChar2 <  strTestChar ? "TRUE" : "FALSE") << std::endl;

    std::cout << "strTestChar2 <=  strTestChar: "
         << (strTestChar2 <=  strTestChar ? "TRUE" : "FALSE") << std::endl;
    std::cout << "strTestChar <=  strTestChar: "
         << (strTestChar <=  strTestChar ? "TRUE" : "FALSE") << std::endl;

    std::cout << "strTestChar2 >  strTestChar: "
         << (strTestChar2 >  strTestChar ? "TRUE" : "FALSE") << std::endl;
    std::cout << "strTestChar2 >=  strTestChar: "
         << (strTestChar2 >=  strTestChar ? "TRUE" : "FALSE") << std::endl;
    std::cout << "strTestChar >=  strTestChar2: "
         << (strTestChar >=  strTestChar2 ? "TRUE" : "FALSE") << std::endl;
    std::cout << "strTestChar >=  strTestChar: "
         << (strTestChar >=  strTestChar ? "TRUE" : "FALSE") << std::endl;

    std::cout << "--------------------------------------------- " <<  std::endl;
    std::cout << "--------------------------------------------- " <<  std::endl;
    std::cout << "--------------- SPECIALIZATION -------------- " <<  std::endl;
    std::cout << "--------------- KBasicString<char> --------- " <<  std::endl;

    int ic = 12345;
    KBasicString<char> strIntc(ic);
    KBasicString<char> str1c("beginning");
    KBasicString<char> str2c = "Continuation";
    KBasicString<char> str3c = "_||_ " + str1c + strIntc + 98765 + str2c + " plus ";
    KBasicString<char> str4c = str2c;
    str4c = str4c;

    std::cout << str3c << " " << str4c << std::endl;

    KBasicString<char> str5c = "string";

    std::cout << "str5c[3] : " << str5c[3] << std::endl;
    str5c[3] = 'I';
    std::cout << "str5c : " << str5c << std::endl;
    std::cout << "str5c.at(3) : " << str5c.at(3) << std::endl;
    std::cout << "str5c > bla? : " << (str5c > "bla" ? "TRUE": "FALSE") << std::endl;
    std::cout << "str5c < bla? : " << (str5c < "bla" ? "TRUE": "FALSE") << std::endl;
    std::cout << "str5c != string ? : "
         << (str5c != "string" ? "TRUE": "FALSE") << std::endl;
    std::cout << "str5c == string ? : "
         << (str5c == "string" ? "TRUE": "FALSE") << std::endl;
    std::cout << "The size of str5c is " << str5c.size() << " bytes.\n";
    std::cout << "The length of str5c is " << str5c.length() << " bytes.\n";
    std::cout << "The first character of str5c is " << str5c.front() << std::endl;
    std::cout << "The last character of str5c is " << str5c.back() << std::endl;

    //test unicode c-tor
    kchar_t unicodeC(1092);
    KBasicString<char> strUnicodeC(unicodeC);

    std::cout << "test unicode. str5c + str6c "
         << str5c + " " + strUnicodeC << std::endl;

    str5c.clear();
    std::cout << "Cleared str5c is: " << "\"" << str5c << "\"" << std::endl;
    std::cout << "Is cleared str5c empty? : "
         << ( str5c.empty() ? "TRUE": "FALSE") << std::endl;

    KBasicString<char> str6c = "The time ";
    std::cout << "str6: " << str6c.append("is now!") << std::endl;
    KBasicString<char> tmpKStrc(" is now again!");
    str6c += tmpKStrc;
    str6c += " Give up yourself unto the moment.";
    std::cout << "str6 : " << str6c << std::endl;

    KBasicString<char> str7c = "Nora";
    std::cout << "str7c : " << str7c << std::endl;
    str7c.reverse();
    std::cout << "str7c reversed: " << str7c << std::endl;

    //test the the out of range exception
    /*
    try
    {
        std::cout << "str7c[-2] : " << str7c[-2] << std::endl;
    }
    catch (const out_of_range &oor)
    {
        cerr << oor.what() << std::endl;
    }
    */
    const KBasicString<char> const_strc = "Blafasel";
    //const_strc[5] = 'S'; /* Ist nicht komplierbar und zu Recht */
    KBasicString<char> non_const_strc = "Blafasel";
    non_const_strc[5] = 'Z';
    std::cout << non_const_strc << std::endl;


    std::cout << "--------------------------------------------- " <<  std::endl;
    std::cout << "--------------------------------------------- " <<  std::endl;
    std::cout << "--------------- SPECIALIZATION -------------- " <<  std::endl;
    std::cout << "--------------- KBasicString<kchar_t> --------- " <<  std::endl;


/*
                                 //UTF8         UTF16     UTF32         Decimal
#define LATIN_Z_DECIMAL   122    //0x7A         0x007A    0x0000007A    122
#define RUSSIAN_F_DECIMAL 1092   //0xD1 0x84    0x0444    0x00000444    1092
#define RUSSIAN_S_DECIMAL 1089   //0xD1 0x81    0x0441    0x00000441    1089
#define RUSSIAN_B_DECIMAL 1073   //0xD0 0xB1    0x0431    0x00000431    1073
    //Chinese/Japanese character: 字 unicode: U+5b57 //UTF8   bytestring:'\xe5\xad\x97' decimal: 23383
*/
    kchar_t z(122); //z
    kchar_t f(1092); //ф

    KBasicString<kchar_t> strUnicode(z);
    strUnicode.append(f);
    strUnicode += 1089; //с
    strUnicode = strUnicode + (KBasicString<kchar_t>)23383; //字

    std::cout << "strUnicode: " << strUnicode << std::endl;
    std::cout << "strUnicode.c_str ():  " << strUnicode.c_str () << std::endl;

    std::cout << "-------KBasicString<kchar_t>  UTF-8 c-tor ---------- " <<  std::endl;

    KBasicString<kchar_t> strUnicode2("Юся");
    std::cout << "strUnicode2: " << strUnicode2 << std::endl;
    std::cout << "strUnicode2.c_str ():  " << strUnicode2.c_str () << std::endl;

    KBasicString<kchar_t> strUnicode3("Юля Julya");
    std::cout << "strUnicode3: " << strUnicode3 << std::endl;
    std::cout << "strUnicode3.c_str ():  " << strUnicode3.c_str () << std::endl;

    KBasicString<kchar_t> strUnicode4("zфс字");
    std::cout << "strUnicode4: " << strUnicode4 << std::endl;
    std::cout << "strUnicode4.c_str ():  " << strUnicode4.c_str () << std::endl;


#endif
    return 0;
}












